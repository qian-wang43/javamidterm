package ca.sheridancollege.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ca.sheridancollege.beans.Restaurant;
import ca.sheridancollege.dao.DAO;

/**
 * Servlet implementation class HomeController
 */
@WebServlet("/HomeController")
public class HomeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private DAO dao = new DAO();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeController() {
        super();
        dao.populate();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String submit=request.getParameter("submit");
		switch (submit) {
			case "Search":
				String keywords=request.getParameter("keywords");
				String price=request.getParameter("price");
				String rating=request.getParameter("rating");
				List<Restaurant> restaurantList=dao.queryRestaurantByCriteria(keywords, price, rating);
				System.out.print(restaurantList.get(0).getName());		
				request.setAttribute("restaurantList", restaurantList);
				request.setAttribute("requirement", submit);
				request.getRequestDispatcher("display.jsp").forward(request,response);
				break;
			case "Show All":
				List<Restaurant> restaurantList1=dao.queryRestaurant();
				request.setAttribute("restaurantList", restaurantList1);
				request.setAttribute("requirement", submit);
				request.getRequestDispatcher("display.jsp").forward(request,response);
				break;
			case "Add New":
				request.getRequestDispatcher("add.jsp").forward(request,response);
				
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("name");
		String price=request.getParameter("price");
		String rating=request.getParameter("rating");
		String type=request.getParameter("type");
		String note=request.getParameter("note");
		dao.addNew(name, type, note, price, rating);
		List<Restaurant> restaurantList=dao.queryRestaurant();
		request.setAttribute("restaurantList", restaurantList);
		request.setAttribute("requirement", "add");
		request.getRequestDispatcher("display.jsp").forward(request,response);
		
	}

}
