package ca.sheridancollege.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedQuery(name="Restaurant.byCriteria", query="from Restaurant where note like :note and price= :price and rating= :rating")
@NamedQuery(name="Restaurant.all", query="from Restaurant order by id desc")
public class Restaurant {

	@Id
	@GeneratedValue
	private int id;
	private String name;
	private String note;
	private String type;
	private String price;
	private String rating;

	public Restaurant(String name, String type, String note, String price, String rating) {
		this.name=name;
		this.type=type;
		this.note=note;
		this.price=price;
		this.rating=rating;
	}
}
