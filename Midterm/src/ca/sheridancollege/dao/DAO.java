package ca.sheridancollege.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import ca.sheridancollege.beans.Restaurant;

public class DAO {

	SessionFactory sessionFactory = new Configuration().configure("ca/sheridancollege/config/hibernate.cfg.xml")
			.buildSessionFactory();

	public void populate() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		session.save(new Restaurant("Mondello Ristorante","Italian","Gluten Free Options.","$$","****"));
		session.save(new Restaurant("LaVinia Restaurant","Spanish","Vegetarian Friendly","$$$","***"));
		session.save(new Restaurant("Union Social Eatery","American","Local cuisine","$$","**"));
				
		session.getTransaction().commit();
		session.close();
	}
	
	public List<Restaurant> queryRestaurantByCriteria(String keywords, String price, String rating) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query = session.getNamedQuery("Restaurant.byCriteria");
		
		query.setParameter("note", "%"+keywords+"%");
		query.setParameter("price", price);
		query.setParameter("rating", rating);
	
		List<Restaurant> restaurantList = query.getResultList();

		session.getTransaction().commit();
		session.close();
		
		return restaurantList;
	}
	
	public List<Restaurant> queryRestaurant( ) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Query query = session.getNamedQuery("Restaurant.all");
		
		List<Restaurant> restaurantList = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
		
		return restaurantList;
	}
	
	public void addNew(String name, String type, String note, String price, String rating) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		session.save(new Restaurant(name,type,note,price,rating));
				
		session.getTransaction().commit();
		session.close();
		
	}

}
