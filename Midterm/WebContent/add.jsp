<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add New</title>
</head>
<body>
	<h1>Add a new restaurant</h1>
	<form method="post" action="HomeController">
		<p>Name: <input type="text" name="name"/></p>
		<p>Cuisine: <input type="text" name="type"/></p>
		<p>Price: <select name="price">
			  <option value ="$" selected="selected">$</option>
			  <option value ="$$">$$</option>
			  <option value="$$$" >$$$</option>
			  <option value="$$$$">$$$$</option>
			  <option value="$$$$$">$$$$$</option>
			</select>	
		</p>
		<p>Rating: <select name="rating">
			  <option value ="*" selected="selected">*</option>
			  <option value ="**">**</option>
			  <option value="***" >***</option>
			  <option value="****">****</option>
			  <option value="****">*****</option>
			</select>	
		</p>
		<p>Note: <input type="text" name="note"></p>
		<input type="submit" value="Add New"/>
	</form>
	<p><a href="index.jsp">Back to Home</a></p>
	
</body>
</html>