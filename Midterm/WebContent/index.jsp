<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Midterm</title>
</head>
<body>
<h1>Welcome!</h1>
<form method="get" action="HomeController">
	<p>Keywords of note: <input type="text" name="keywords"/></p>
	<p>Price: <select name="price">
		  <option value ="$" selected="selected">$</option>
		  <option value ="$$">$$</option>
		  <option value="$$$" >$$$</option>
		  <option value="$$$$">$$$$</option>
		  <option value="$$$$$">$$$$$</option>
		</select>	
	</p>
	<p>Rating: <select name="rating">
		  <option value ="*" selected="selected">*</option>
		  <option value ="**">**</option>
		  <option value="***" >***</option>
		  <option value="****">****</option>
		  <option value="****">*****</option>
		</select>	
	</p>
	<input type="submit" name="submit" value="Search"/>
	<input type="submit" name="submit" value="Show All"/>
	<input type="submit" name="submit" value="Add New"/>
</form>

</body>
</html>