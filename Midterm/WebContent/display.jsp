<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Result</title>
</head>
<body>
	<p><a href="index.jsp">Back to Home</a></p>
	<c:choose>
		<c:when test="${requestScope.restaurantList.size()==0}">
			<h1>No such restaurant</h1>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${requestScope.requirement=='Search'}">
					<h1>No such restaurant</h1>
				</c:when>
				<c:when test="${requestScope.requirement=='Show All'}">
					<h1>All the restaurants are:</h1>
				</c:when>
				<c:when test="${requestScope.requirement=='add'}">
					<h1>Your restaurant is added successfully. All the restaurants are:</h1>
				</c:when>
			</c:choose>
			<c:forEach var="item" items="${requestScope.restaurantList}">
				<h2>${item.name}</h2>
				Cuisine: ${item.type} | Price: ${item.price} | Start Rating: ${item.rating}<br/>
				Note: ${item.note}
			</c:forEach>
		</c:otherwise>
	</c:choose>
</body>
</html>